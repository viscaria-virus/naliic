import { defineStore } from "pinia";
import { readonly, ref } from "vue";

// 定义全局状态-数据库条目
export const useEntryStore=defineStore('entry',()=>{
    // 初始化状态值
    const value=ref<string>('');
    // 状态更新方法
    function update(query:string){
        value.value=query;
    }
    return {
        value:readonly(value),
        update
    }
})