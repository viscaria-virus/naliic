import { defineStore } from "pinia";
import { readonly, ref } from "vue";

// 定义全局状态-查询结果
export const useQueryStore=defineStore('query',()=>{
    // 初始化状态值
    const value=ref<string[]>([]);
    // 状态更新方法
    function update(result:string[]){
        value.value=result;
    }
    return {
        value:readonly(value),
        update
    }
});
