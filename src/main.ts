import { createApp } from 'vue';
import { createPinia } from 'pinia';
import { createRouter,createWebHashHistory } from 'vue-router';
import App from './App.vue';
// 导入路由配置
import { routes } from './router';

const app=createApp(App);
// 使用Pinia状态管理插件
const pinia=createPinia();
app.use(pinia);
// 使用HASH模式路由
const router=createRouter({
    history:createWebHashHistory(),
    routes
});
app.use(router);
app.mount('#app');
