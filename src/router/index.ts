// 动态导入主页面
const HomePage=()=>import('../pages/Home.vue');
// 动态导入浏览页面
const BrowsePage=()=>import('../pages/Browse.vue');
// 动态导入搜索页面
const SearchPage=()=>import('../pages/Search.vue');
// 动态导入下载页面
const DownloadPage=()=>import('../pages/Download.vue');
// 动态导入查询页面
const QueryPage=()=>import('../pages/Query.vue');
// 动态导入条目页面
const EntryPage=()=>import('../pages/Entry.vue');

// 路由配置
export const routes=[
    // 主页面
    {
        path:'/',
        component:HomePage
    },
    // 浏览页面
    {
        path:'/browse',
        component:BrowsePage
    },
    // 搜索页面
    {
        path:'/search',
        component:SearchPage
    },
    // 下载页面
    {
        path:'/download',
        component:DownloadPage
    },
    // 查询页面
    {
        path:'/query',
        component:QueryPage
    },
    // 条目页面
    {
        path:'/entry',
        component:EntryPage
    }
];
