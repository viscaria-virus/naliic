# NALIIC

## 简介

全面的核酸配体互作信息数据库，与采用B/S架构的数据库网站不同，是一个采用C/S架构的桌面应用程序。

## 产品特色

+ 简单易用且信息全面。
+ 同时支持Windows和MacOS的跨平台桌面应用程序。
+ 本地化数据库实现比网站数据库更快的搜索查询。

## 技术架构

+ 使用vite搭建项目开发环境，通过插件和配置实现了全量热更新开发。
+ 使用electron框架搭建的桌面应用程序，数据库在electron主进程中加载，为界面提供API访问。
+ 使用Vue框架基于TypeScript搭建的GUI界面，绝大部分组件基于框架原生封装实现，浏览页面引用了element+组件。
+ 采用Sass进行CSS样式开发，采用了vw/vh/%以实现响应式布局。
+ 使用pinia进行状态管理，存储了查询结果和指定数据库条目。
+ 使用vue-router配置了页面路由，采用hash模式获得了较好的性能。
+ 基于哈希表结构进行数据库搜索，基于编辑距离计算字符串相似度进行查询匹配。
+ 设置定时任务定期通过axios进行异步请求以实现自动更新。
+ 采用JSON格式作为数据存储文件。

## 项目结构

```shell
├── LICENSE 开源许可证
├── auto-imports.d.ts 组件库的自动导入类型声明文件
├── build.js 打包构建脚本
├── components.d.ts 组件类型声明文件
├── forge.config.js 打包配置(electron)
├── index.html 项目前端入口页面
├── package-make.json 打包依赖配置(electron)
├── package.json 项目依赖配置
├── src
│   ├── App.vue 应用入口组件
│   ├── components 
│   │   ├── ComplexList.vue 复合物结构条目列表组件
│   │   ├── DownloadButton.vue 下载按钮组件
│   │   ├── Introduce.vue 简介文本组件
│   │   ├── LigandList.vue 配体条目列表组件
│   │   ├── LinkButton.vue 链接按钮组件
│   │   ├── Navigator.vue 导航栏组件
│   │   ├── SubmitButton.vue 提交按钮组件
│   │   └── TitleText.vue 标题文本组件
│   ├── main.ts 应用入口脚本(vue)
│   ├── pages
│   │   ├── Browse.vue 浏览页面
│   │   ├── Download.vue 下载页面
│   │   ├── Entry.vue 条目页面
│   │   ├── Home.vue 主页面
│   │   ├── Query.vue 查询页面
│   │   └── Search.vue 搜索页面
│   ├── router
│   │   └── index.ts 页面路由配置(vue-router)
│   ├── static
│   │   ├── docs.json 文档
│   │   └── url.json 下载资源配置
│   ├── stores
│   │   ├── entry.ts 数据库条目状态
│   │   └── query.ts 查询结果状态
│   └── vite-env.d.ts 开发环境类型声明文件
├── src-electron
│   ├── blast 本地blast工作目录
│   ├── main.ts 应用入口脚本(electron)
│   ├── preload.ts 预加载脚本
│   └── static
│       ├── dataset.json 数据库存档
│       └──datasource.json 数据源配置
├── tsconfig.json 编译配置(TypeScript)
├── tsconfig.node.json 编译配置(TypeScript-Node)
└── vite.config.ts 开发配置(vite)
```
