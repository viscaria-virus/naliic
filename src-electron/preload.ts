import { contextBridge,ipcRenderer } from 'electron';

// 向渲染进程暴露接口
contextBridge.exposeInMainWorld('electronAPI',{
    browse:()=>ipcRenderer.invoke('browse'),
    search:(method:string,match:string|string[])=>ipcRenderer.invoke('search',method,match),
    match:(query:string)=>ipcRenderer.invoke('match',query),
    onUpdate:(callback)=>ipcRenderer.on('update',callback)
});
