{
  "version": "1.2",
  "dbname": "database",
  "dbtype": "Nucleotide",
  "db-version": 5,
  "description": "database",
  "number-of-letters": 68434,
  "number-of-sequences": 1607,
  "last-updated": "2023-05-28T05:51:00",
  "number-of-volumes": 1,
  "bytes-total": 1277198,
  "bytes-to-cache": 38076,
  "files": [
    "database.ndb",
    "database.nhr",
    "database.nin",
    "database.not",
    "database.nsq",
    "database.ntf",
    "database.nto"
  ]
}
