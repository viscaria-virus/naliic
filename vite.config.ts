import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import AutoImport from 'unplugin-auto-import/vite';
import Components from 'unplugin-vue-components/vite';
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers';
import electron from 'vite-electron-plugin';
import { rmSync,readdirSync,cpSync } from 'node:fs';
import path from 'node:path';

// 配置electron源代码目录和编译结果目录
const electronSrc:string='src-electron';
const electronDist:string='dist-electron';
// 启动开发服务器和热更新时清除原有的electron编译结果
if(readdirSync('.').includes(electronDist)){
  rmSync(electronDist,{recursive:true});
}
// 启动开发服务器和热更新时复制electron项目中的静态资源
const staticDir:string=path.join(electronSrc,'static');
for(const item of readdirSync(staticDir)){
  cpSync(
    path.join(staticDir,item),
    path.join(electronDist,item),
    {recursive:true}
  );
}
const blastDir:string=path.join(electronSrc,'blast');
cpSync(blastDir,path.join(electronDist,'blast'),{recursive:true});

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),// 配置Vue热更新
    electron({
      include:[electronSrc]
    }),// 配置electron热更新
    AutoImport({
      resolvers:[ElementPlusResolver()]
    }),// 配置element+组件自动导入
    Components({
      resolvers:[ElementPlusResolver()]
    })// 配置element+组件自动导入
  ]
});
